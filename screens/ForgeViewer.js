import React, { Component } from 'react';
import { ScrollView, View, WebView } from 'react-native';
import { Tile, List, ListItem, Button, FormLabel, FormInput } from 'react-native-elements';

class ForgeViewer extends Component {

  render() {
    let HTML = `
        <head>
          <link rel="stylesheet" href="https://developer.api.autodesk.com/viewingservice/v1/viewers/style.min.css?v=v2.16" type="text/css">
          <script src="https://developer.api.autodesk.com/viewingservice/v1/viewers/three.min.js?v=v2.16"></script>
          <script src="https://developer.api.autodesk.com/viewingservice/v1/viewers/viewer3D.js?v=v2.16"></script>
        </head>
        <div id="myContent">
            Viewer Goes Here
        </div>
        <div id="forgeViewer" ></div>
    `;
    let jsCode = `
        document.querySelector('#myContent').style.backgroundColor = 'red';
        alert(Autodesk);

        var svfURL = "https://lmv-models.s3.amazonaws.com/toy_plane/toy_plane.svf";
        var viewer;
        function onSuccess() {
            viewer.setBackgroundColor(0,0,0,0,0,0);
        }

        function initializeViewer() {
            var viewerDiv = document.querySelector('#forgeViewer');
            viewer = new Autodesk.Viewing.Private.GuiViewer3D(viewerDiv, {});
		
            var options = {
                env: "Local",
                useConsolidation: true,
                useADP: false,
            }

            Autodesk.Viewing.Initializer( options, function() {
                viewer.start(svfURL, options, onSuccess);            
            });
        }
		initializeViewer();
    `;

    return (
      <WebView
        source={{ html: HTML }}
        style={{marginTop: 20}}
        injectedJavaScript={jsCode}
        javaScriptEnabled={true}
      />
    );
  }
}

export default ForgeViewer;
